FROM node:18-alpine3.15

RUN mkdir /srv/app && chown node:node /srv/app

RUN npm i -g @adonisjs/cli

USER node

WORKDIR /srv/app

COPY --chown=node:node package.json package-lock.json ./

RUN npm install --quiet

RUN mkdir -p node_modules

COPY . .

RUN cp .env.example .env

USER root
EXPOSE 3333

CMD [ "npm", "run", "dev" ]